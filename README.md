# BSU Fall 2016 CS 120 Art Show Project 

## Name
"Stardust" by Old Man Joe

## Description
This was a Jython project submitted to the Ball State University CS 120 Art Show for the Fall 2016 semester. It was judged by my peers as one of the best projects in my section (http://www.cs.bsu.edu/homepages/dllargent/cs120/artShow/2016Fall/index.html) and by a panel of faculty to be "Best in show for Code Quality" (http://www.cs.bsu.edu/homepages/dllargent/cs120/artShow/2016Fall/results.html). This project used a variety of techniques to create the collage, including, bilinear resizing, color removal, pixel mirroring, and color shifting.

## Visuals
![Collage of 9 tiled images of a terrestrial planet with a red ring above a starry field with a green cloud. The images are altered in different ways featuring mirrors, color shifts, and scaling.](CS120_Project3_RHiggins.jpg "Stardust")