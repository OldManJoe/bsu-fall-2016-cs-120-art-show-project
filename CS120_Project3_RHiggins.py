#Robert Higgins "Old Man Joe"
#25 October 2016
#CS120
#Project 3
#Version 1.2.2

#Copies a source image onto a target image starting at the given X, Y coordinate, or at 0,0 by default.
#Returns the edited image without editing the original pictures
def copy(source, target, startX = 0, startY = 0) :
  width = getWidth(source)
  height = getHeight(source)
  newTarget = duplicatePicture(target)
  
  for x in range(width) :
    if x >= getWidth(newTarget) : break
    for y in range(height) :
      if y >= getHeight(newTarget) : break
      setColor(getPixel(newTarget, x + startX, y + startY), getColor(getPixel(source, x, y)))
      
  return newTarget

#Rotates a given picture 90 degrees counterclockwise
#Returns the edited image without editing the original picture
def rotateLeft(picture) :
  width = getWidth(picture)
  height = getHeight(picture)
  newPicture = makeEmptyPicture(height, width)
  
  for x in range(width) :
    for y in range(height) :
      setColor(getPixel(newPicture, y, width - 1 - x), getColor(getPixel(picture, x, y)))
 
  return newPicture

#Rotates a given picture 90 degrees clockwise
#Returns the edited image without editing the original picture
def rotateRight(picture) :
  width = getWidth(picture)
  height = getHeight(picture)
  newPicture = makeEmptyPicture(height, width)
  
  for x in range(width) :
    for y in range(height) :
      setColor(getPixel(newPicture, height - 1 - y, x), getColor(getPixel(picture, x, y)))
 
  return newPicture

#Flips an image horizontally
#Returns the edited image without editing the original picture
def horizontalFlip(picture) :
  width = getWidth(picture)
  height = getHeight(picture)
  newPicture = makeEmptyPicture(width, height)
  
  for x in range(width) :
    for y in range(height) :
      setColor(getPixel(newPicture, width - 1 - x, y), getColor(getPixel(picture, x, y)))
 
  return newPicture

#Flips an image vertically
#Returns the edited image without editing the original picture
def verticalFlip(picture) :
  width = getWidth(picture)
  height = getHeight(picture)
  newPicture = makeEmptyPicture(width, height)
  
  for x in range(width) :
    for y in range(height) :
      setColor(getPixel(newPicture, x, height - 1 - y), getColor(getPixel(picture, x, y)))
 
  return newPicture

#Mirrors the left side of the image to the right side
#Returns the edited image without editing the original picture
def mirrorLeftToRight(picture) :
  width = getWidth(picture)
  height = getHeight(picture)
  newPicture = makeEmptyPicture(width, height)
  
  for x in range(width/2) :
    for y in range(height) :
      setColor(getPixel(newPicture, x, y), getColor(getPixel(picture, x, y)))
      setColor(getPixel(newPicture, width - 1 - x, y), getColor(getPixel(picture, x, y)))
 
  if height % 2 == 1 :
   for y in range(height) :
     setColor(getPixel(newPicture, width/2, y), getColor(getPixel(picture, width/2, y)))
  
  return newPicture

#Mirrors the right side of the image to the left side
#Returns the edited image without editing the original picture
def mirrorRightToLeft(picture) :
  width = getWidth(picture)
  height = getHeight(picture)
  newPicture = makeEmptyPicture(width, height)
  
  for x in range(width/2) :
    for y in range(height) :
      setColor(getPixel(newPicture, x, y), getColor(getPixel(picture, width - 1 - x, y)))
      setColor(getPixel(newPicture, width - 1 - x, y), getColor(getPixel(picture, width - 1 - x, y)))
  
  if height % 2 == 1 :
    for y in range(height) :
      setColor(getPixel(newPicture, width/2, y), getColor(getPixel(picture, width/2, y)))
  
  return newPicture

#Mirrors the top half of the image to the bottom half
#Returns the edited image without editing the original picture
def mirrorTopToBottom(picture) :
  width = getWidth(picture)
  height = getHeight(picture)
  newPicture = makeEmptyPicture(width, height)
  
  for x in range(width) :
    for y in range(height/2) :
      setColor(getPixel(newPicture, x, y), getColor(getPixel(picture, x, y)))
      setColor(getPixel(newPicture, x, height - 1 - y), getColor(getPixel(picture, x, y)))
 
  if height % 2 == 1 :
   for x in range(width) :
     setColor(getPixel(newPicture, x, height/2), getColor(getPixel(picture, x, height/2)))

  return newPicture

#Mirrors the bottom half of the image to the top half
#Returns the edited image without editing the original picture
def mirrorBottomToTop(picture) :
  width = getWidth(picture)
  height = getHeight(picture)
  newPicture = makeEmptyPicture(width, height)
  
  for x in range(width) :
    for y in range(height/2) :
      setColor(getPixel(newPicture, x, y), getColor(getPixel(picture, x, height - 1 - y)))
      setColor(getPixel(newPicture, x, height - 1 - y), getColor(getPixel(picture, x, height - 1 - y)))
 
  if height % 2 == 1 :
    for x in range(width) :
      setColor(getPixel(newPicture, x, height/2), getColor(getPixel(picture, x, height/2)))
     
  return newPicture

#Scales an image by the given scale factor using nearest neighbor
#Returns the edited image without editing the original picture
def scaleImage(picture, scale) :
  width = getWidth(picture)
  height = getHeight(picture)
  newWidth = getWidth(picture) * scale
  newHeight = getHeight(picture) * scale

  x_ratio = width/float(newWidth)
  y_ratio = height/float(newHeight)

  newPicture = makeEmptyPicture(int(newWidth), int(newHeight))

  for x in range(0, newWidth):
    for y in range(0, newHeight):
      newPixel = getPixel(newPicture, x, y)

      px = floor(x*x_ratio);
      py = floor(y*y_ratio);

      oldPixel = getPixel(picture, int(px), int(py))
      setColor(newPixel, getColor(oldPixel))

  return newPicture

#Scales an image by the given scale factor using bilinear interpolation
#Returns the edited image without editing the original picture
def resizeBilinear(picture, scale) :
  pixels = getPixels(picture)
  width = getWidth(picture)
  height = getHeight(picture)
  newWidth = float(getWidth(picture) * scale)
  newHeight = float(getHeight(picture) * scale)
  
  newPicture = makeEmptyPicture(int(newWidth), int(newHeight))
  
  x_ratio = float(width-1)/newWidth
  y_ratio = float(height-1)/newHeight
  
  for x in range(newWidth) :
    for y in range(newHeight) :
      x_diff = (x_ratio * x) - int(x_ratio * x)
      y_diff = (y_ratio * y) - int(y_ratio * y)
      
      index = int(y_ratio * y) * width + int(x_ratio * x)
      
      A = pixels[index]
      B = pixels[index+1]
      C = pixels[index+width]
      D = pixels[index+width+1]

      # blue element
      # Yb = Ab(1-w)(1-h) + Bb(w)(1-h) + Cb(h)(1-w) + Db(wh)
      newBlue = (getBlue(A))*(1-x_diff)*(1-y_diff) + (getBlue(B))*(x_diff)*(1-y_diff) + (getBlue(C))*(y_diff)*(1-x_diff) + (getBlue(D))*(x_diff*y_diff)

      # green element
      # Yg = Ag(1-w)(1-h) + Bg(w)(1-h) + Cg(h)(1-w) + Dg(wh)
      newGreen = (getGreen(A))*(1-x_diff)*(1-y_diff) + (getGreen(B))*(x_diff)*(1-y_diff) + (getGreen(C))*(y_diff)*(1-x_diff) + (getGreen(D))*(x_diff*y_diff)

      # red element
      # Yr = Ar(1-w)(1-h) + Br(w)(1-h) + Cr(h)(1-w) + Dr(wh)
      newRed = (getRed(A))*(1-x_diff)*(1-y_diff) + (getRed(B))*(x_diff)*(1-y_diff) + (getRed(C))*(y_diff)*(1-x_diff) + (getRed(D))*(x_diff*y_diff)

      setColor(getPixel(newPicture, x, y), makeColor(newRed, newGreen, newBlue))
      
  return newPicture

#Halves red values
#Returns the edited image without editing the original picture
def halfRed(picture) :
  newPicture = duplicatePicture(picture)
  
  for pixel in getPixels(newPicture) :
    setRed(pixel, getRed(pixel)/2)
      
  return newPicture

#Halves green values
#Returns the edited image without editing the original picture
def halfGreen(picture) :
  newPicture = duplicatePicture(picture)
  
  for pixel in getPixels(newPicture) :
    setGreen(pixel, getGreen(pixel)/2)
      
  return newPicture

#Halves blue values
#Returns the edited image without editing the original picture
def halfBlue(picture) :
  newPicture = duplicatePicture(picture)
  
  for pixel in getPixels(newPicture) :
    setBlue(pixel, getBlue(pixel)/2)
      
  return newPicture

#Halves all color values
#Returns the edited image without editing the original picture
def halfColor(picture) :
  return halfRed(halfGreen(halfBlue(picture)))

#Negates all color values
#Returns the edited image without editing the original picture
def negative(picture) :
  newPicture = duplicatePicture(picture)
  
  for pixel in getPixels(newPicture) :
    setColor(pixel, makeColor(255 - getRed(pixel), 255 - getGreen(pixel), 255 - getBlue(pixel)))
      
  return newPicture

#Removes background color from an image before copying it to a desired location
#Returns the edited image without editing the original pictures
def removeColorAndCopy(source, colorToIgnore, target, startX=0, startY=0) :
  width = getWidth(source)
  height = getHeight(source)
  newTarget = duplicatePicture(target)
  
  for x in range(width) :
    if x + startX >= getWidth(newTarget) : break
    for y in range(height) :
      if y + startY >= getHeight(newTarget) : break
      
      color = getColor(getPixel(source, x, y))
      if distance(color, colorToIgnore) > 140 : setColor(getPixel(newTarget, x + startX, y + startY), color) #140 was found through testing to be most effective
      
  return newTarget

#Removes background color from an image before copying it to a desired location
#This version uses a distance comparison half of previous verison
#Returns the edited image without editing the original pictures
def removeColorAndCopy2(source, colorToIgnore, target, startX=0, startY=0) :
  width = getWidth(source)
  height = getHeight(source)
  newTarget = duplicatePicture(target)
  
  for x in range(width) :
    if x + startX >= getWidth(newTarget) : break
    for y in range(height) :
      if y + startY >= getHeight(newTarget) : break
      
      color = getColor(getPixel(source, x, y))
      if distance(color, colorToIgnore) > 70 : setColor(getPixel(newTarget, x + startX, y + startY), color) #70 was found through testing to be most effective
      
  return newTarget

#Rotates RGB values using Green values for Red, Blue for Green, and Red for Blue.
#Returns the edited image without editing the original picture
def rgbShiftRight(picture) :
  newPicture = duplicatePicture(picture)
  
  for pixel in getPixels(newPicture) :
    setColor(pixel, makeColor(getBlue(pixel), getRed(pixel), getGreen(pixel)))

  return newPicture

#Rotates RGB values using Blue values for Red, Red for Green, and Green for Blue.
#Returns the edited image without editing the original picture
def rgbShiftLeft(picture) :
  newPicture = duplicatePicture(picture)
  
  for pixel in getPixels(newPicture) :
    setColor(pixel, makeColor(getGreen(pixel), getBlue(pixel), getRed(pixel)))

  return newPicture

#Adds a signature from an image with a white background changinging it to red and adding it to a picture
#Returns the edited image without editing the original picture
def addSignature(picture, signature, startX, startY) :
  width = getWidth(signature)
  height = getHeight(signature)
  newTarget = duplicatePicture(picture)
  
  for x in range(width) :
    if x + startX >= getWidth(newTarget) : break
    for y in range(height) :
      if y + startY >= getHeight(newTarget) : break
      
      if distance(getColor(getPixel(signature, x, y)), white) > 140 : setColor(getPixel(newTarget, x + startX, y + startY), red)
      
  return newTarget

#creates, displays, and saves the collage
def collage() :
  WIDTH = 700 #700 is the width of the image
  HEIGHT = 515 #515 is the height of the image
  X1 = Y1 = 0
  X2 = WIDTH/3 
  X3 = WIDTH*2/3
  Y2 = HEIGHT/3 
  Y3 = HEIGHT*2/3
  
  background = makeEmptyPicture(WIDTH, HEIGHT)
  background = copy(makePicture(getMediaPath("Starfield (small).jpg")), background)
  planet1 = makePicture(getMediaPath("Planet 2 (small).jpg"))
  planet2 = mirrorRightToLeft(planet1)
  planet3 = halfBlue(resizeBilinear(planet1, 1.5))
  planet4 = negative(planet1)
  planet5 = rgbShiftRight(rotateRight(planet1))
  planet6 = mirrorBottomToTop(halfColor(planet1))
  planet7 = mirrorTopToBottom(mirrorLeftToRight(planet1))
  planet8 = verticalFlip(rgbShiftLeft(planet1))
  signature = makePicture(getMediaPath("Signature.jpg"))
  
  background = removeColorAndCopy(planet1, makeColor(0,255,0), background, X1, Y1)
  background = removeColorAndCopy(planet2, makeColor(0,255,0), background, X1, Y2)
  background = removeColorAndCopy(planet3, makeColor(0,255,0), background, WIDTH/4, HEIGHT/4) #special location because scaled up
  background = removeColorAndCopy(planet4, makeColor(255,0,255), background, X3, Y1)
  background = removeColorAndCopy(planet5, makeColor(0,0,255), background, X2, Y3)
  background = removeColorAndCopy2(planet6, makeColor(0,255/2,0), background, X3, Y2)
  background = removeColorAndCopy(planet7, makeColor(0,255,0), background, X2, Y1)
  background = removeColorAndCopy(planet8, makeColor(255,0,0), background, X1, Y3)
  background = removeColorAndCopy(planet1, makeColor(0,255,0), background, X3, Y3)
  background = addSignature(background, signature, 500, 415)
  show(background)
  writePictureTo(background, "CS120_Project3_RHiggins.jpg")
